// evento de carga


window.addEventListener('load', function () {
  setTimeout(function () {
    document.getElementById('preloader').remove()
  })
})


// active scroll
const navChange = document.getElementById('header')
const ActiveBacktotop = document.getElementById('backtotop')

window.addEventListener('scroll', () => {
  if (window.scrollY >= 1) {
    navChange.classList.add('header-active')
    ActiveBacktotop.classList.add('backtotop-active')
  } else {
    navChange.classList.remove('header-active')
    ActiveBacktotop.classList.remove('backtotop-active')
  }
})

// input file styles

function cambiar(){
  let changeNameFile  = document.getElementById('file-upload').files[0].name;
  document.getElementById('modal-info').innerHTML = changeNameFile;
}

// navburger

const showMenu = (toggleId, navId,iconBurger) => {
  const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        icon = document.getElementById(iconBurger)

  if (toggle && nav) {
    toggle.addEventListener('click', () => {
      nav.classList.toggle('main-menu-show')
      icon.classList.toggle('fa-times')
    })


  }
}
showMenu('burger', 'main-menu','burgerIcon')

//ocultar menuburger al hacer clic

const links = Array.from(document.querySelectorAll('.main-menu__link')),
      nav = document.getElementById('main-menu')
      

for (let i = 0; i < links.length; i++) {
  if (links) {
    links[i].addEventListener('click', () => {
      nav.classList.toggle('main-menu-show')
      
    })
  }
}

// dejar subrayado la seccion en la que estoy

//smooth scroll animation

const scroll = new SmoothScroll('.ss-animation', {
  speed: 800
})



// progress bar


const bar1 = document.getElementById('bar1'),
      bar2 = document.getElementById('bar2'),
      bar3 = document.getElementById('bar3'),
      bar4 = document.getElementById('bar4'),
      bar5 = document.getElementById('bar5'),
      bar6 = document.getElementById('bar6')

function animateBar(e , mx){
  const smallBP = matchMedia('(max-width:712px)')
  const mediumBP = matchMedia('(min-width:713px)')
  if (scrollY >= 900 && scrollY <= 1850 && smallBP.matches) {
    e.style.animation = "animatebar 3s ease"
    e.style.maxWidth = mx
  } else if(scrollY >= 300 && scrollY <= 1500 && mediumBP.matches){
    e.style.animation = "animatebar 3s ease"
    e.style.maxWidth = mx
    
  }else {
    e.style.animation = "none"
    e.style.maxWidth = "0%"
  }
}

addEventListener('scroll', () => animateBar(bar1 , bar1.textContent))
addEventListener('scroll', () => animateBar(bar2 , bar2.textContent))
addEventListener('scroll', () => animateBar(bar3 , bar3.textContent))
addEventListener('scroll', () => animateBar(bar4 , bar4.textContent))
addEventListener('scroll', () => animateBar(bar5 , bar5.textContent))
addEventListener('scroll', () => animateBar(bar6 , bar6.textContent))
// imprimir moda6
// Get DOM Eleme6ts
const modal = document.querySelector('#my-modal');
const modalBtn = document.querySelector('#modal-btn');
const modalBtn2 = document.querySelector('#modal-btn2');
const closeBtn = document.querySelector('.close');

// Events
modalBtn.addEventListener('click', openModal);
modalBtn2.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

// Open
function openModal() {
  modal.style.display = 'block';
}

// Close
function closeModal() {
  modal.style.display = 'none';
}

// Close If Outside Click
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = 'none'
    nav.classList.toggle('main-menu-show')
  }
}

// animation cards

const btn1 = document.getElementById('cardbtn1')
const btn2 = document.getElementById('cardbtn2')
const btn3 = document.getElementById('cardbtn3')
const btn4 = document.getElementById('cardbtn4')
const btn5 = document.getElementById('cardbtn5')
const btn6 = document.getElementById('cardbtn6')

const nav1 = document.getElementById('cardnav1')
const nav2 = document.getElementById('cardnav2')
const nav3 = document.getElementById('cardnav3')
const nav4 = document.getElementById('cardnav4')
const nav5 = document.getElementById('cardnav5')
const nav6 = document.getElementById('cardnav6')

const activeCard = (btn , nav) =>{
    btn.addEventListener('click',()=>{
      btn.classList.toggle('pulsed')
      nav.classList.toggle('expand')
    })
  }
        
activeCard(btn1 , nav1)
activeCard(btn2 , nav2)
activeCard(btn3 , nav3)
activeCard(btn4 , nav4)
activeCard(btn5 , nav5)
activeCard(btn6 , nav6)
  
