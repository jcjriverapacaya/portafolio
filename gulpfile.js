
    var gulp = require('gulp');
    var browserSync= require('browser-sync').create();
    var autoprefixer = require('gulp-autoprefixer');
    var sass = require('gulp-sass');
    var babel = require('gulp-babel');
    var concat = require('gulp-concat');
    var imagemin = require('gulp-imagemin');
    var uglify = require('gulp-uglify-es').default;
    var rename = require('gulp-rename');
    var cleanCSS = require('gulp-clean-css');
    var del = require('del');
    
    var paths = {
      styles: {
        src: 'src/scss/**/*.scss',
        dest: 'src/assets/css/'
      },
      scripts: {
        src: 'src/js/**/*.js',
        dest: 'src/assets/js/'
      },
      pages:{
        src:'src/pages/**/*.html',
        dest:'src/assets/'
      },
      media: {
        src: 'src/media/**/*.*',
        dest:'src/assets/media/'
      }
    };

    function clean() {
      return del([ 'assets' ]);
    }

    function pages(){
      return gulp.src(paths.pages.src)
        .pipe(gulp.dest(paths.pages.dest));
    }
    function styles() {
      return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCSS())
        .pipe(rename({
          basename: 'styles',
          suffix: '.min'
        }))
        .pipe(gulp.dest(paths.styles.dest));
    }
    function images(){
      return gulp.src(paths.media.src)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: false},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.media.dest));
    }
    function scripts() {
      return gulp.src(paths.scripts.src, { sourcemaps: true })
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.scripts.dest));
    }
    
    function watch() {
      browserSync.init({
        server: {
          baseDir: "./src/assets/"
        }
      });
      gulp.watch(paths.pages.src,pages);
      gulp.watch(paths.scripts.src, scripts);
      gulp.watch(paths.styles.src, styles);
      gulp.watch(paths.media.src, images) ;
      gulp.watch([paths.pages.dest, paths.scripts.dest , paths.styles.dest, paths.media.dest]).on('change',browserSync.reload);
    }
    
   exports.clean = clean;
   exports.pages = pages;
   exports.styles = styles;
   exports.images = images;
   exports.scripts = scripts;
   exports.watch = watch;
  
  
  var build = gulp.series(clean, gulp.parallel(pages,styles,images, scripts,watch));
  gulp.task('default',build);